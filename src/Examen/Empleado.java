package Examen;

import java.util.Calendar;

/**
 * @author Enrique Avivar Rosino
 * @since 14/02/2014
 * @see Administrativo
 */

public class Empleado {
        /**
         * Método generado por Refractor. Se usa para sustituir un trozo de código por la utilización de este
         * método.
         * @param empleado
         * @return 
         */
        public boolean igual(Empleado empleado) {
                boolean igual;
                igual = this.getNombre().equals(empleado.getNombre()) &&
                this.getSalario() == empleado.getSalario() &&
                this.getAnioAlta() == empleado.getAnioAlta();
                return igual;
        }
        
        //Variables a usar por el programa
        public enum EstadoEmpleado {ACTIVO, EXCEDENCIA} //Enumeración de los estados del empleado.
        private static final int INICIO_EMPRESA = 2014; //Creación en la empresa. Predeterminado a 2014.
        private String nombre; //Aquí se guarda el nombre del empleado
        private float salario; //Aquí se guarda el salario del empleado (con decimales)
        private int anioAlta; //Aquí se guarda el año de alta
        private EstadoEmpleado estado; //Identifica uno de los dos posibles estado del empleado.
        
        /**
         * Constructor de la clase empleado. Se introducen el nombre, el salario, y el año de alta, y se valora
         * si los datos pueden o no ser correctos. Si son incorrectos se lanza una excepción.
         * @param nombre
         * @param salario
         * @param anyoAlta
         * @throws IllegalArgumentException 
         */
        public Empleado(String nombre, float salario, int anyoAlta) throws IllegalArgumentException
        {
                if (nombre == null || nombre.equals(""))
                        throw new IllegalArgumentException("No se puede crear el empleado. "+
												"El nombre no es significativo.");	
				
                if (salario<=0)
                        throw new IllegalArgumentException("No se puede crear el empleado. "+
												"El salario tiene que ser positivo.");
			
                if (!esCorrectoAnyo(anyoAlta))
                        throw new IllegalArgumentException("No se puede crear el empleado. "+
												"El a�o de alta no es v�lido.");
		
                this.nombre = nombre;
                this.salario = salario;
                setAnyoAlta(anyoAlta);
                estado = EstadoEmpleado.ACTIVO;
        }

        /**
         * Devuelve la fecha de creación de la empresa.
         * @return INICIO_EMPRESA
         */
        public static int getINICIO_EMPRESA() {
                return INICIO_EMPRESA;
        }

        /**
         * Devuelve el string nombre, con el nombre del empleado.
         * @return nombre
         */
        public String getNombre() {
                return nombre;
        }

        /**
         * Se introduce el nombre del empleado.
         * @param nombre
         */
        public void setNombre(String nombre) {
                this.nombre = nombre;
        }

        /**
         * Se introduce el salario del empleado.
         * @param salario
         */
        public void setSalario(float salario) {
                this.salario = salario;
        }

        /**
         * Devuelve el int anioAlta, dónde se especifica el año de alta del empleado.
         * @return anioAlta
         */
        public int getAnioAlta() {
                return anioAlta;
        }

        /**
         * Introduce el año de alta del empleado.
         * @param anioAlta
         */
        public void setAnioAlta(int anioAlta) {
                this.anioAlta = anioAlta;
        }

        /**
         * Devuelve el estado del empleado.
         * @return estado
         */
        public EstadoEmpleado getEstado() {
                return estado;
        }

        /**
         * Introduce el estado del empleado, que debe ser ACTIVO o EXCEDENCIA.
         * @param estado 
         */
        public void setEstado(EstadoEmpleado estado) {
                this.estado = estado;
        }
        
        /**
         * Determina si el año es correcto. Si es incorrecto devuelve false, y si es correcto devuelve true.
         * @param anio
         */
        private boolean esCorrectoAnyo(int anio)
        {
                int anyoActual = Calendar.getInstance().get(Calendar.YEAR);
                if (anio >= getINICIO_EMPRESA() && anio <= anyoActual)
                return true;
                else return false;
        }
        
        /**
         * Establece el año de alta del empleado. Si el año no es válido lanza una excepción.
         * @param anioAlta
         * @throws IllegalArgumentException 
         */
        private void setAnyoAlta(int anioAlta) throws IllegalArgumentException
        {
                if (!esCorrectoAnyo(anioAlta)) 
                throw new IllegalArgumentException("El año que se quiere establecer no es válido");
                this.setAnioAlta(anioAlta);
        }
	
        /**
         * Devuelve el salario del empleado, si es activo.
         */
        public float getSalario() 
        {
        if (getEstado() == EstadoEmpleado.ACTIVO)
                return salario;
        else 
                return 0f;
        }

        /**
         * Incrementa el salario del empleado. Si se hace sobre un empleado con estado EXCEDENCIA, lanza 
         * una excepción.
         * @param subida
         * @throws IllegalStateException 
         */
        public void incSalario(float subida) throws IllegalStateException
        {
        if (getEstado() == EstadoEmpleado.EXCEDENCIA)
                throw new IllegalStateException("No se puede incrementar el salario de un empleado en excedencia");
        
                setSalario(getSalario() + subida); 
        }
	
		
        /**
         * @param obj
         */
        public boolean equals(Object obj)
        {	
                boolean igual=false;
            
                if (obj == null) 
                        return false;
	
                if (this == obj) 
                        return true;
		
                 Empleado empleado = (Empleado) obj;
		
                igual = igual(empleado);
                
                return igual;
        }
        	
}