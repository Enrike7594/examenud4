package Examen;


/**
 * Clase administrativo. Hereda los métodos de Empleado y sobreescribe algunos.
 * @author alumno
 * @since 14/02/2014
 */

public class Administrativo extends Empleado {

        private float factor;//Declaración del float factor.

        /**
         * Constructor de Administrativo. Hace una llamada al constructor de empleado con la palabra super.
         * @param nombre
         * @param salario
         * @param anioAlta 
         */
        public Administrativo (String nombre, float salario, int anioAlta) 
        {
                super(nombre, salario, anioAlta);
        }
	
        /**
         * Devuelve el salario de un Administrativo.
         */
        public float getSalario() 
        {	
                return super.getSalario() + super.getSalario() *  getFactor();
        }

        /**
         * Devuelve el factor.
         * @return Factor
         */
        public float getFactor() {
                return factor;
        }

        /**
         * Introduce el factor con el que trabajar.
         * @param factor 
         */
        public void setFactor(float factor) {
                this.factor = factor;
        }
        
	
		
}
